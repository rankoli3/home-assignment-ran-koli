import MoviesRepository from "../../src/back/repository/movies-repository";
import {RestClient} from "../../src/back/rest-client/RestClient";
import {ApiResponse, MovieDetails} from "../../src/back/repository";

jest.mock('../../src/back/rest-client/RestClient');

describe('MoviesRepository', () => {
    let moviesRepository: MoviesRepository;
    const mockRestClient = new RestClient('www.fake.url');

    beforeEach(() => {
        // Resetting mocks and creating a new instance for each test to ensure isolation
        jest.clearAllMocks();
        moviesRepository = new MoviesRepository(mockRestClient, '?apikey=test');
    });

    describe('searchMovies', () => {
        it('should return a list of movies on successful search', async () => {
            const mockMovies: ApiResponse['Search'] = [
                { Title: 'Test Movie', Year: '2020', imdbID: 'tt1234567', Type: 'movie', Poster: 'test.jpg' },
            ];
            (mockRestClient.request as jest.Mock).mockResolvedValueOnce({ data: { Response: "True", Search: mockMovies }});

            const result = await moviesRepository.searchMovies('Test');
            expect(result).toEqual(mockMovies);
            expect(mockRestClient.request).toHaveBeenCalledWith('get', '?apikey=test&s=Test');
        });

        it('should throw an error if no movies are found', async () => {
            const error = new Error("Couldn't find movies");
            (mockRestClient.request as jest.Mock).mockRejectedValue(error);
            await expect(moviesRepository.searchMovies('Test')).rejects.toThrow(error);
        });
    });

    describe('getMovieDetail', () => {
        it('should return movie details on success', async () => {
            const mockDetails: MovieDetails = {
                Title: 'Test Movie',
                Year: '1980',
                Rated: 'N/A',
                Released: 'N/A',
                Season: 'N/A',
                Episode: 'N/A',
                Genre: 'N/A',
                Director: 'N/A',
                Actors: 'N/A',
                Language: 'N/A',
                Country: 'N/A',
                imdbID: 'N/A',
                Type: 'N/A',
                Poster: 'N/A',
                Plot: 'N/A',
                Response: 'True',
            };
            (mockRestClient.request as jest.Mock).mockResolvedValueOnce({ data: mockDetails });

            const result = await moviesRepository.getMovieDetail('tt1234567');
            expect(result).toEqual(mockDetails);
            expect(mockRestClient.request).toHaveBeenCalledWith('get', '?apikey=test&i=tt1234567');
        });

        it('should throw an error if movie details cannot be found', async () => {
            const error = new Error("Couldn't find movies");
            (mockRestClient.request as jest.Mock).mockRejectedValue(error);
            await expect(moviesRepository.getMovieDetail('tt1234567')).rejects.toThrow(error);
        });
    });
});