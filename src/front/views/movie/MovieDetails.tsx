import React from 'react';
import { createPortal } from 'react-dom';
import {MovieDetails} from "@/back/repository";

interface Props {
    movieDetail: MovieDetails;
    onClose: () => void;
}

const MovieDetailComponent: React.FC<Props> = ({ movieDetail, onClose }) => {
    return createPortal(
        <div className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center pt-10" onClick={onClose}>
            <div className='bg-white p-5 rounded-lg max-w-lg w-full'
                 onClick={(e) => e.stopPropagation()}
            >
                <div className='w-full'>
                    <img src={movieDetail.Poster}
                         alt={movieDetail.Title}
                         loading='eager'
                         className='h-[432px] w-full'
                    />
                </div>

                <h3 className='text-xl font-bold mt-4'>{movieDetail.Title}</h3>
                <p className='mt-2'>{movieDetail.Plot}</p>
                <p className='text-gray-600'>Year: {movieDetail.Year}</p>
                <p className='text-gray-600'>Director: {movieDetail.Director}</p>
                <p className='text-gray-600'>Episode: {movieDetail.Episode}</p>
                <p className='text-gray-600'>Season: {movieDetail.Season} Episode: {movieDetail.Episode}</p>
                <p className='text-gray-600'>Language: {movieDetail.Language} </p>
                <button className='mt-4 py-2 px-4 bg-blue-500 text-white rounded hover:bg-blue-700'
                        onClick={onClose}
                >Close
                </button>
            </div>
        </div>,
        document.body
    );
};

export default MovieDetailComponent;
