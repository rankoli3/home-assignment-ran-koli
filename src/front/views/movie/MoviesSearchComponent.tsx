import React, {useEffect, useState} from 'react';
import MovieDetailsComponent from '@/front/views/movie/MovieDetails.tsx';
import {moviesService} from "@/back/services";
import {MovieDetails} from "@/back/repository";
import {useToast} from "@/front/hooks/useToast.ts";
import {Toast} from "@/front/views/common/Toast.tsx";

interface Movie {
    Id: string;
    Title: string;
    Year: string;
    Type: string;
    Poster: string;
}


const MoviesSearchComponent: React.FC = () => {
    const [movies, setMovies] = useState<Movie[]>([]);
    const [searchTerm, setSearchTerm] = useState('Disney');
    const [selectedMovieDetail, setSelectedMovieDetail] = useState<MovieDetails | null>(null);
    const [inputValue, setInputValue] = useState('');
    const [cache, setCache] = useState<{ [key: string]: Movie[] | MovieDetails }>({});
    const {showToast, toastOpen, toastMessage, toastSeverity, handleClose} = useToast();


    useEffect(() => {
        const fetchMovies = async () => {
            if (cache[searchTerm]) {
                setMovies(cache[searchTerm] as Movie[]);
            } else {
                const response = await moviesService.searchMovies(searchTerm, (msg,) => showToast(msg, 'warning'));
                if (!response) {
                    setDefaultValue();
                    return;
                }
                const mappedMovies = response.map((movie) => ({
                    Id: movie.imdbID,
                    Title: movie.Title,
                    Year: movie.Year,
                    Type: movie.Type,
                    Poster: movie.Poster,
                }));
                setMovies(mappedMovies);
                setCache((prevCache) => ({
                    ...prevCache,
                    [searchTerm]: mappedMovies,
                }));
            }
        };

        fetchMovies();
    }, [searchTerm, cache]);

    const handleMovieClick = async (movieId: string) => {
        if (cache[movieId]) {
            setSelectedMovieDetail(cache[movieId] as MovieDetails);
        } else {
            const detail = await moviesService.getMovieDetail(movieId);
            setSelectedMovieDetail(detail);
            setCache((prevCache) => ({
                ...prevCache,
                [movieId]: detail,
            }));
        }
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        setSearchTerm(inputValue);
    };

    const setDefaultValue = () => {
        setSearchTerm('disney');
        setInputValue('');
    };

    return (
        <div className='p-5'>
            <form onSubmit={handleSubmit}
                  className='flex space-x-2'
            >
                <input
                    className='flex-1 p-2 border border-gray-300 rounded-md'
                    type='text'
                    value={inputValue}
                    onChange={(e) => setInputValue(e.target.value)}
                    placeholder='Search movies...'
                />
                <button
                    type='submit'
                    className='px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-700'
                >
                    Search
                </button>
            </form>
            <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 gap-4 mt-4'>
                {movies.map((movie) => (
                    <div
                        key={movie.Id}
                        className='cursor-pointer border rounded-lg overflow-hidden shadow-lg'
                        onClick={() => handleMovieClick(movie.Id)}
                    >
                        <img className='w-full h-64 object-fill'
                             src={movie.Poster}
                             alt={movie.Title}
                        />
                        <div className='p-4'>
                            <p className='text-lg font-bold'>{movie.Title}</p>
                            <p className='text-gray-600'>{movie.Year} - {movie.Type}</p>
                        </div>
                    </div>
                ))}
            </div>
            {selectedMovieDetail && (
                <MovieDetailsComponent movieDetail={selectedMovieDetail}
                                       onClose={() => setSelectedMovieDetail(null)}
                />
            )}
            <Toast open={toastOpen}
                   message={toastMessage}
                   severity={toastSeverity}
                   onClose={handleClose}
            />
        </div>
    );
};

export default MoviesSearchComponent;
