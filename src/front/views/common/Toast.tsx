import React from 'react';
import { Snackbar, Alert } from '@mui/material';

interface ToastProps {
    open: boolean;
    message: string;
    severity: 'error' | 'warning' | 'info' | 'success';
    onClose: () => void;
}

export const Toast: React.FC<ToastProps> = ({ open, message, severity, onClose }) => {
    return (
        <Snackbar open={open} autoHideDuration={6000} onClose={onClose}>
            <Alert onClose={onClose} severity={severity} sx={{ width: '100%' }}>
                {message}
            </Alert>
        </Snackbar>
    );
};
