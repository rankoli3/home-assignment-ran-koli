import { useState } from 'react';

type ToastSeverity = 'error' | 'warning' | 'info' | 'success';

interface UseToastReturn {
    showToast: (message: string, severity: ToastSeverity) => void;
    toastOpen: boolean;
    toastMessage: string;
    toastSeverity: ToastSeverity;
    handleClose: () => void;
}

export const useToast = (): UseToastReturn => {
    const [toastOpen, setToastOpen] = useState(false);
    const [toastMessage, setToastMessage] = useState('');
    const [toastSeverity, setToastSeverity] = useState<ToastSeverity>('info');

    const showToast = (message: string, severity: ToastSeverity) => {
        setToastMessage(message);
        setToastSeverity(severity);
        setToastOpen(true);
    };

    const handleClose = () => {
        setToastOpen(false);
    };

    return { showToast, toastOpen, toastMessage, toastSeverity, handleClose };
};
