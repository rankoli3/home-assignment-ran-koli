# Harmon.ie home assignment Ran-Koli
## Install pnpm
```
npm install -g pnpm
```
## Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm run dev
```

### Compiles and minifies for production
```
pnpm run build
```

### Run your unit tests
```
pnpm run test:unit
```

### Lints and fixes files
```
pnpm run lint
```

### Gitlab Pages Link

[Movies Project](https://home-assignment-ran-koli-rankoli3-695d7633ae9c1e353e1b2f509c1be.gitlab.io/)
