import MoviesRepository from "@/back/repository/movies-repository.ts";
import {ApiResponse, MovieDetails} from "@/back/repository";

/**
 * Activity Service
 */
export default class {
    private moviesRepository: MoviesRepository;

    constructor(moviesRepository: MoviesRepository) {
        this.moviesRepository = moviesRepository;
    }

    /**
     * Find Movies
     * @returns {ApiResponse.Search[]} - containing a list of movies
     */
    async searchMovies(title: string,  onError: (msg: string) => void): Promise<ApiResponse['Search'] | null> {
        try {
            return await this.moviesRepository.searchMovies(title);
        } catch (e) {
            onError(`Failed to get Movies for the search term: ${title}`);
            return null;
        }

    }

    /**
     * get movie by id
     * @returns {MovieDetails} - promise containing a movie details
     */
    async getMovieDetail(movieId: string):  Promise<MovieDetails> {
        try {
            return await this.moviesRepository.getMovieDetail(movieId);
        } catch (e) {
            console.log(`Failed to get Movie with id: ${movieId}`);
            throw e;
        }
    }
}