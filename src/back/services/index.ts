import { moviesRepository } from '@/back/repository';
import MoviesService from '@/back/services/movies-service.ts';

/**
 * Responsible for creating all services singletons and exposing them
 */
// setup services

const moviesService = new MoviesService(moviesRepository);

Object.freeze(moviesService);

export {
    moviesService,
};
