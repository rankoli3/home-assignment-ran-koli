import axios, {AxiosInstance, AxiosResponse, Method} from 'axios';

enum HttpMethod {
    GET = 'get',
    POST = 'post',
}
Object.freeze(HttpMethod);

export {
    HttpMethod,
}

/** Rest client */
export class RestClient {
    private axiosInstance: AxiosInstance;

    /**
     * Create a Rest client
     * @param {String} baseURL - Base URL to be used for all REST requests
     * @param {Object} headers - Global headers
     */
    constructor(baseURL: string, headers?: object) {
        this.axiosInstance = axios.create({
            baseURL,
            headers
        });
    }

    /**
     * Make a RESTful request
     * @param {Method} method - HTTP Method
     * @param {string} endpoint - REST endpoint
     * @param {Object} params - Request parameters
     * @param {Object} data - Request body data
     * @returns {Promise<AxiosResponse<T>>} - A response promise
     */
    async request<T>(method: Method, endpoint: string, params?: object, data?: object): Promise<AxiosResponse<T>> {
        try {
            return await this.axiosInstance.request<T>({
                method,
                url: endpoint,
                params,
                data,
            });
        } catch (error) {
            // Handle or throw the error appropriately
            console.error('Error fetching data: ', error);
            throw error;
        }
    }
}