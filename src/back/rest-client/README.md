# `rest-client`

A Super Rest Client.

## Usage

```js
      import { RestClient, HttpMethod } from '/rest-client';

      const restClient = new RestClient();
      try {
          const response = await restClient.request < T > (HttpMethod.GET, 'endPoint');
        console.info(response);
      } catch (e) {
        console.error(e);
      }
```
