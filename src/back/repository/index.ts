import MoviesRepository from "@/back/repository/movies-repository.ts";
import { RestClient } from "@/back/rest-client/RestClient.ts";

const defaultBaseURL = 'https://www.omdbapi.com/';
const defaultEndPoint = '?apikey=8871ad36';

/**
 * Responsible for creating all repository singletons and exposing them
 */
// initiate data sources
const restClient = new RestClient(defaultBaseURL);

// setup repositories
const moviesRepository = new MoviesRepository(restClient, defaultEndPoint);

Object.freeze(moviesRepository);

export {
    moviesRepository,
};

export interface ApiResponse {
    Search: Array<{
        Title: string;
        Year: string;
        imdbID: string;
        Type: string;
        Poster: string;
    }>;
    totalResults: string;
    Response: string;
}

export interface MovieDetails {
    "Title": string,
    "Year": string,
    "Rated": string,
    "Released": string,
    "Season": string,
    "Episode": string,
    "Genre": string,
    "Director": string,
    "Actors": string,
    "Language": string,
    "Country": string,
    "imdbID": string,
    "Type": string,
    Poster: string;
    Plot: string;
    "Response": string
}