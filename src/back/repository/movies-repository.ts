import {HttpMethod, RestClient} from '@/back/rest-client/RestClient.ts';
import {ApiResponse, MovieDetails} from "@/back/repository/index.ts";

/**
 * Movies Repository
 */
export default class {
    private restClient: RestClient;
    private endPoint: string;

    constructor(restClient: RestClient, endPoint: string) {
        this.restClient = restClient;
        this.endPoint = endPoint;
    }

    /**
     * Search movies by title
     * @returns {ApiResponse.Search[]} - promise containing a list of movies
     */
    async searchMovies(title: string): Promise<ApiResponse['Search']> {
        const response = await this.restClient.request<ApiResponse>(HttpMethod.GET, `${this.endPoint}&s=${title}`);
        const data = response.data;
        if (!data || data.Response !== "True") throw new Error("Couldn't find movies");

        return data.Search;
    }

    /**
     * get movie by id
     * @returns {MovieDetails} - promise containing a movie details
     */
    async getMovieDetail(movieId: string): Promise<MovieDetails> {
        const response = await this.restClient.request<MovieDetails>(HttpMethod.GET, `${this.endPoint}&i=${movieId.replace(/ /g, '')}`);
        const data = response.data;
        if (!data || data.Response !== "True") throw new Error("Couldn't find movie");

        return data;
    }
}

